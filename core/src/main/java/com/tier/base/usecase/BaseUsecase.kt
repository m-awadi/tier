package com.tier.base.usecase

import androidx.lifecycle.LiveData

abstract class BaseUsecase<Input : BaseInput, Output : BaseOutput> {
    abstract suspend fun execute(input: Input): LiveData<Output>
}
interface BaseInput
interface BaseOutput