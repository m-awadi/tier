package com.tier.base.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

abstract class BaseViewModel<S : BaseViewState, A : BaseAction, R : BaseResult> : ViewModel() {
    protected abstract var internalViewState: S

    protected abstract fun handle(action: A): LiveData<R>
    protected abstract fun reduce(result: R): S

    private val nextAction = MutableLiveData<A>()

    val viewState = Transformations.map(Transformations.switchMap(nextAction) {
        handle(it)
    }) {
        internalViewState = reduce(it)
        internalViewState
    }

    fun dispatch(action: A) {
        nextAction.value = action
    }
}
interface BaseAction
interface BaseViewState
interface BaseResult
