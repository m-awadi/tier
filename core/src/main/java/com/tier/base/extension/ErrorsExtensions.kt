package com.tier.base.extension

import okio.IOException


fun Throwable.isNetworkError(): Boolean = this is IOException