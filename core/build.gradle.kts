import com.android.build.gradle.internal.dsl.BaseFlavor

plugins {
    id(GradlePluginId.ANDROID_LIBRARY)
    id(GradlePluginId.KOTLIN_ANDROID)
    id(GradlePluginId.KOTLIN_ANDROID_EXTENSIONS)
    id(GradlePluginId.KOTLIN_KAPT)
}

android {
    compileSdkVersion(AndroidConfig.COMPILE_SDK_VERSION)

    defaultConfig {
        minSdkVersion(AndroidConfig.MIN_SDK_VERSION)
        targetSdkVersion(AndroidConfig.TARGET_SDK_VERSION)

        versionCode = AndroidConfig.VERSION_CODE
        versionName = AndroidConfig.VERSION_NAME
        testInstrumentationRunner = AndroidConfig.TEST_INSTRUMENTATION_RUNNER
        buildConfigFieldFromGradleProperty("apiBaseUrl")

    }

    buildTypes {
        getByName(BuildType.RELEASE) {
            isMinifyEnabled = BuildTypeRelease.isMinifyEnabled
            proguardFiles("proguard-android.txt", "proguard-rules.pro")
        }

        getByName(BuildType.DEBUG) {
            isMinifyEnabled = BuildTypeDebug.isMinifyEnabled
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {

    api(LibraryDependency.APP_COMPACT)
    api(LibraryDependency.NAVIGATION_FRAGMENT_KTX)
    api(LibraryDependency.NAVIGATION_UI_KTX)
    api(LibraryDependency.CALLIGRAPHY)
    api(LibraryDependency.CORE_KTX)
    api(LibraryDependency.COROUTINES_ANDROID)
    api(LibraryDependency.COROUTINES_CORE)
    api(LibraryDependency.FRAGMENT_KTX)
    api(LibraryDependency.GSON)
    api(LibraryDependency.K_ANDROID)
    api(LibraryDependency.koinAndroid)
    api(LibraryDependency.koinViewModel)
    api(LibraryDependency.KOTLIN)
    api(LibraryDependency.LIFECYCLE_EXTENSIONS)
    api(LibraryDependency.LIFECYCLE_RUNTIME)
    api(LibraryDependency.LIFECYCLE_VIEW_MODEL_KTX)
    api(LibraryDependency.LIVEDATA_KTX)
    api(LibraryDependency.LOGGING_INTERCEPTOR)
    api(LibraryDependency.LOTTIE)
    api(LibraryDependency.MAPS)
    api(LibraryDependency.MAPS_UTILS)
    api(LibraryDependency.MATERIAL)
    api(LibraryDependency.RECYCLER_VIEW)
    api(LibraryDependency.RETROFIT)
    api(LibraryDependency.RETROFIT_GSON_CONVERTER)
    api(LibraryDependency.CONSTRAINT_LAYOUT)
    api(LibraryDependency.SWIPE_TO_REFRESH)
    addTestDependencies()

}

fun BaseFlavor.buildConfigFieldFromGradleProperty(gradlePropertyName: String) {
    val propertyValue = project.properties[gradlePropertyName] as? String
    checkNotNull(propertyValue) { "Gradle property $gradlePropertyName is null" }

    val androidResourceName = "GRADLE_${gradlePropertyName.toSnakeCase()}".toUpperCase()
    buildConfigField("String", androidResourceName, propertyValue)
}

fun String.toSnakeCase() = this.split(Regex("(?=[A-Z])")).joinToString("_") { it.toLowerCase() }