object CoreVersion {
    const val COROUTINES = "1.3.2"
    const val KOTLIN = "1.3.61"
    const val NAVIGATION = "2.2.0"
}
