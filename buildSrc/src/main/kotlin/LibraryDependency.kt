private object LibraryVersion {
    const val APP_COMPACT = "1.1.0"
    const val CALLIGRAPHY = "0.1.5"
    const val CONSTRAINT_LAYOUT = "1.1.3"
    const val CORE_KTX = "1.1.0"
    const val FRAGMENT_KTX = "1.1.0"
    const val GOOGLE_PLAY_SERVICES = "16.1.0"
    const val GSON = "2.8.6"
    const val K_ANDROID = "0.8.8@aar"
    const val KOIN = "2.0.0-beta-1"
    const val LIFECYCLE = "1.1.1"
    const val LIFECYCLE_VIEW_MODEL_KTX = "2.2.0-alpha01"
    const val LOGGING_INTERCEPTOR = "4.3.0"
    const val LOTTIE = "3.3.1"
    const val MAP_UTILS = "0.5"
    const val MATERIAL = "1.1.0-alpha09"
    const val PLAY_CORE = "1.6.4"
    const val RECYCLER_VIEW = "1.1.0"
    const val RETROFIT = "2.7.1"
    const val SWIPE_TO_REFRESH = "1.0.0"
}

object LibraryDependency {
    const val APP_COMPACT = "androidx.appcompat:appcompat:${LibraryVersion.APP_COMPACT}"
    const val CALLIGRAPHY = "com.github.takahirom.downloadable.calligraphy:downloadable-calligraphy:${LibraryVersion.CALLIGRAPHY}"
    const val CORE_KTX = "androidx.core:core-ktx:${LibraryVersion.CORE_KTX}"
    const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${CoreVersion.COROUTINES}"
    const val COROUTINES_CORE = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${CoreVersion.COROUTINES}"
    const val FRAGMENT_KTX = "androidx.fragment:fragment-ktx:${LibraryVersion.FRAGMENT_KTX}"
    const val GSON = "com.google.code.gson:gson:${LibraryVersion.GSON}"
    const val K_ANDROID = "com.pawegio.kandroid:kandroid:${LibraryVersion.K_ANDROID}"
    const val koinAndroid = "org.koin:koin-android:${LibraryVersion.KOIN}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${LibraryVersion.KOIN}"
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${CoreVersion.KOTLIN}"
    const val LIFECYCLE_EXTENSIONS = "android.arch.lifecycle:extensions:${LibraryVersion.LIFECYCLE}"
    const val LIFECYCLE_RUNTIME = "androidx.lifecycle:lifecycle-runtime:${LibraryVersion.LIFECYCLE_VIEW_MODEL_KTX}"
    const val LIFECYCLE_VIEW_MODEL_KTX = "androidx.lifecycle:lifecycle-viewmodel-ktx:${LibraryVersion.LIFECYCLE_VIEW_MODEL_KTX}"
    const val LIVEDATA_KTX = "androidx.lifecycle:lifecycle-livedata-ktx:${LibraryVersion.LIFECYCLE_VIEW_MODEL_KTX}"
    const val LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:${LibraryVersion.LOGGING_INTERCEPTOR}"
    const val LOTTIE = "com.airbnb.android:lottie:${LibraryVersion.LOTTIE}"
    const val MAPS = "com.google.android.gms:play-services-maps:${LibraryVersion.GOOGLE_PLAY_SERVICES}"
    const val MAPS_UTILS = "com.google.maps.android:android-maps-utils:${LibraryVersion.MAP_UTILS}"
    const val MATERIAL = "com.google.android.material:material:${LibraryVersion.MATERIAL}"
    const val NAVIGATION_FRAGMENT_KTX = "androidx.navigation:navigation-fragment-ktx:${CoreVersion.NAVIGATION}"
    const val NAVIGATION_UI_KTX = "androidx.navigation:navigation-ui-ktx:${CoreVersion.NAVIGATION}"
    const val PLAY_CORE = "com.google.android.play:core:${LibraryVersion.PLAY_CORE}"//required by split application
    const val RECYCLER_VIEW = "androidx.recyclerview:recyclerview:${LibraryVersion.RECYCLER_VIEW}"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${LibraryVersion.RETROFIT}"
    const val RETROFIT_GSON_CONVERTER = "com.squareup.retrofit2:converter-gson:${LibraryVersion.RETROFIT}"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${LibraryVersion.CONSTRAINT_LAYOUT}"
    const val SWIPE_TO_REFRESH = "androidx.swiperefreshlayout:swiperefreshlayout:${LibraryVersion.SWIPE_TO_REFRESH}"
}
