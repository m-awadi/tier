private object TestLibraryVersion {
    const val ANDROID_X_TEST = "2.1.0"
    const val ESPRESSO = "3.2.0"
    const val FRAGMENT_TEST = "1.2.0"
    const val JUNIT = "4.13"
    const val KLUENT = "1.59"
    const val LIVE_DATA_TEST = "1.1.1"
    const val MOCKK = "1.9.3"
    const val TEST_SUPPORT = "1.0.2"
}

object TestLibraryDependency {
    const val ANDROID_X_CORE_TESTING = "androidx.arch.core:core-testing:${TestLibraryVersion.ANDROID_X_TEST}"
    const val COROUTINES_TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${CoreVersion.COROUTINES}"
    const val JUNIT = "junit:junit:${TestLibraryVersion.JUNIT}"
    const val KLUENT = "org.amshove.kluent:kluent:${TestLibraryVersion.KLUENT}"
    const val LIVE_DATA_TEST ="com.jraska.livedata:testing-ktx:${TestLibraryVersion.LIVE_DATA_TEST}"
    const val MOCKK = "io.mockk:mockk:${TestLibraryVersion.MOCKK}"
    const val TEST_RULES = "com.android.support.test:rules:${TestLibraryVersion.TEST_SUPPORT}"
    const val TEST_RUNNER = "com.android.support.test:runner:${TestLibraryVersion.TEST_SUPPORT}"
}
