import kotlin.reflect.full.memberProperties

private const val FEATURE_PREFIX = ":feature_"

@Suppress("unused")
object ModuleDependency {
    const val APP = ":app"
    const val FEATURE_FLEET = ":feature_fleet"
    const val LIBRARY_CORE = ":core"
    const val LIBRARY_TEST_UTILS = ":library_test_utils"


    fun getAllModules() = ModuleDependency::class.memberProperties
        .filter { it.isConst }
        .map { it.getter.call().toString() }
        .toSet()

    fun getDynamicFeatureModules() = getAllModules()
        .filter { it.startsWith(FEATURE_PREFIX) }
        .toSet()
}
