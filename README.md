# Tier keeper
Thanks for checking out this coding task, here are few tips to help navigate the project.
##### IDE versions:
Android Studio version : 3.5.3
Android Studio Kotlin plugin version : 1.3.61-release-Studio3.5-1
#### Project structure: 
The project consists of 4 gradle module:
  - `App`: Entry Point and host navigation graph.
  - `Core`: hosts core classes across features along with common dependencies.
  - `BuildSrc`: kotlin DSL gradle module to share dependency versions and configurations among modules.
  - `Feature_fleet`: feature module for fleet feature. 
  - `Library_test_utils`: unit test helper library .

The project overall architecture is using simplified version of MVI + Clean architecture, coroutine with livedata to perform asynchronous operations and publish events.