package com.tier.feature.fleet.data.dataSources

import com.tier.feature.fleet.data.converter.FleetDataConverter
import com.tier.feature.fleet.data.retrofit.service.TierRetrofitService
import com.tier.feature.fleet.domain.model.FleetEntity

internal class FleetRemoteDataSource(
    private val TierRetrofitService: TierRetrofitService,
    private val fleetDataConverter: FleetDataConverter
) :
    FleetDataSource {

    override suspend fun retrieveFleet(
    ): List<FleetEntity> = TierRetrofitService.getScootersFleet(
    ).let { fleetDataConverter.convert(it) }
}
