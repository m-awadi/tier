package com.tier.feature.fleet

import com.tier.createNetworkClient
import com.tier.feature.fleet.data.converter.FleetDataConverter
import com.tier.feature.fleet.data.converter.FleetDataRemoteConverter
import com.tier.feature.fleet.data.dataSources.FleetDataSource
import com.tier.feature.fleet.data.dataSources.FleetRemoteDataSource
import com.tier.feature.fleet.data.repository.FleetRepositoryImpl
import com.tier.feature.fleet.data.retrofit.service.TierRetrofitService
import com.tier.feature.fleet.domain.repository.FleetRepository
import com.tier.feature.fleet.domain.usecase.RetrieveFleetUseCase
import com.tier.feature.fleet.presentation.fleetList.FleetListViewModel
import com.tier.feature.fleet.presentation.fleetmap.FleetMapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

fun injectFeature() = loadFeature

private val loadFeature by lazy {
    loadKoinModules(
        viewModelModule,
        useCaseModule,
        repositoryModule,
        dataSourceModule,
        networkModule
    )
}

val viewModelModule: Module = module {
    viewModel { FleetListViewModel(retrieveFleetUseCase = get()) }
    viewModel { FleetMapViewModel() }
}

val useCaseModule: Module = module {
    factory {
        RetrieveFleetUseCase(
            fleetRepository = get()
        )
    }
}

val repositoryModule: Module = module {
    single { FleetRepositoryImpl(fleetRemoteDataSource = get()) } bind FleetRepository::class
}

val dataSourceModule: Module = module {
    single {
        FleetRemoteDataSource(
            TierRetrofitService = get(),
            fleetDataConverter = get()
        )
    } bind FleetDataSource::class

    single { FleetDataRemoteConverter() } bind FleetDataConverter::class

}

val networkModule: Module = module {
    single { retrofit.create(TierRetrofitService::class.java) }
}

private val retrofit: Retrofit =
    createNetworkClient(com.tier.base.BuildConfig.GRADLE_API_BASE_URL, BuildConfig.DEBUG)




