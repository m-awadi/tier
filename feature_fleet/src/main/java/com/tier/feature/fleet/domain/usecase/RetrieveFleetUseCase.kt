package com.tier.feature.fleet.domain.usecase

import androidx.lifecycle.liveData
import com.tier.base.extension.isNetworkError
import com.tier.base.usecase.BaseInput
import com.tier.base.usecase.BaseOutput
import com.tier.base.usecase.BaseUsecase
import com.tier.feature.fleet.domain.model.FleetEntity
import com.tier.feature.fleet.domain.model.FleetState
import com.tier.feature.fleet.domain.repository.FleetRepository

internal class RetrieveFleetUseCase(
    private val fleetRepository: FleetRepository
) : BaseUsecase<RetrieveFleetUseCase.Input, RetrieveFleetUseCase.Output>() {
    override suspend fun execute(input: Input) = liveData {
        emit(Output.Loading)
        emit(retrieveSync(input))
    }

    private suspend fun retrieveSync(input: Input): Output {
        return try {

            val fleet = fleetRepository.retrieveFleet()

            val result = when (input.filter) {
                is Input.FleetFilter.None -> fleet
                is Input.FleetFilter.BatteryFilter -> fleet.filter { it.battery <= input.filter.minBattery }
                is Input.FleetFilter.FleetStateFilter -> fleet.filter { it.state == input.filter.fleetState }
            }

            if (result.isEmpty()) {
                Output.Empty
            } else {
                Output.Success(result)
            }

        } catch (throwable: Throwable) {
            return if (throwable.isNetworkError()) {
                Output.Error.NetworkError
            } else {
                Output.Error.UnexpectedError(throwable)
            }

        }
    }


    data class Input(val filter: FleetFilter = FleetFilter.None) : BaseInput {

        sealed class FleetFilter {
            object None : FleetFilter()
            data class FleetStateFilter(val fleetState: FleetState) : FleetFilter()
            data class BatteryFilter(val minBattery: Int) : FleetFilter()

        }
    }

    sealed class Output : BaseOutput {
        object Loading : Output()
        data class Success(val fleet: List<FleetEntity>) : Output()
        object Empty : Output()
        sealed class Error : Output() {
            object NetworkError : Output.Error()
            data class UnexpectedError(val throwable: Throwable) : Output.Error()
        }
    }

}
