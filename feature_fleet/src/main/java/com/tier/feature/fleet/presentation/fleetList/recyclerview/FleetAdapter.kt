package com.tier.feature.fleet.presentation.fleetList.recyclerview

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pawegio.kandroid.inflateLayout
import com.tier.base.extension.setOnDebouncedClickListener
import com.tier.feature.fleet.R
import com.tier.feature.fleet.domain.model.FleetEntity

internal class FleetAdapter :
    ListAdapter<FleetEntity, FleetAdapter.ScooterViewHolder>(diffCallback){

    private var onDebouncedClickListener: ((x: FleetEntity) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScooterViewHolder {
        return ScooterViewHolder(
            parent.context.inflateLayout(R.layout.fragment_fleet_list_item, parent, false), onDebouncedClickListener
        )

    }

    override fun onBindViewHolder(holder: ScooterViewHolder, position: Int) {
        with(holder) {
            bind(getItem(position))
        }
    }

    fun setOnDebouncedClickListener(listener: (x: FleetEntity) -> Unit) {
        this.onDebouncedClickListener = listener
    }

    internal inner class ScooterViewHolder(
         view: View,
        onDebouncedClickListener: ((x: FleetEntity) -> Unit)?
    ) : RecyclerView.ViewHolder(view) {

        private val icon: ImageView by lazy {
            itemView.findViewById<ImageView>(R.id.fleet_icon)
        }
        private val model: TextView by lazy {
            itemView.findViewById<TextView>(R.id.model)
        }
        private val batteryLevel: TextView by lazy {
            itemView.findViewById<TextView>(R.id.battery_indicator)
        }

        init {
            itemView.setOnDebouncedClickListener {

                (itemView.tag as FleetEntity?)?.let {
                    onDebouncedClickListener?.invoke(it)
                }
            }
        }

        fun bind(fleetEntity: FleetEntity) {
            itemView.tag = fleetEntity
            icon.setImageResource(R.drawable.scooter)
            model.text = fleetEntity.model
            batteryLevel.text = "${fleetEntity.battery}%"

        }

    }

    companion object {

        private val diffCallback = object : DiffUtil.ItemCallback<FleetEntity>() {
            override fun areItemsTheSame(
                oldItem: FleetEntity,
                newItem: FleetEntity
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: FleetEntity,
                newItem: FleetEntity
            ): Boolean =
                oldItem == newItem
        }

    }

}
