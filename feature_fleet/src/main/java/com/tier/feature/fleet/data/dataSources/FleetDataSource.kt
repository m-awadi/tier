package com.tier.feature.fleet.data.dataSources

import com.tier.feature.fleet.domain.model.FleetEntity

internal interface FleetDataSource {
    suspend fun retrieveFleet(
    ): List<FleetEntity>
}
