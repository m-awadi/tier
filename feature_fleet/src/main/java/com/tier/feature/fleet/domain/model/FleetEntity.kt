package com.tier.feature.fleet.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FleetEntity(
    val id: String,
    val state: FleetState,
    val location: LocationEntity,
    val battery: Int,
    val model: String
) : Parcelable
