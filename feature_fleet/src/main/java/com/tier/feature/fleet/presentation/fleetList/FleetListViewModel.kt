package com.tier.feature.fleet.presentation.fleetList

import androidx.lifecycle.Transformations
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.tier.base.liveData.SingleLiveEvent
import com.tier.base.viewmodel.BaseAction
import com.tier.base.viewmodel.BaseResult
import com.tier.base.viewmodel.BaseViewModel
import com.tier.base.viewmodel.BaseViewState
import com.tier.feature.fleet.domain.model.FleetEntity
import com.tier.feature.fleet.domain.model.FleetState
import com.tier.feature.fleet.domain.usecase.RetrieveFleetUseCase

internal class FleetListViewModel(
    private val retrieveFleetUseCase: RetrieveFleetUseCase
) : BaseViewModel<FleetListViewModel.ViewState, FleetListViewModel.Action, FleetListViewModel.Result>() {

    override var internalViewState = ViewState()

    val effect = SingleLiveEvent<Effect>()

    override fun handle(action: Action) = liveData(viewModelScope.coroutineContext) {
        when (action) {
            is Action.LoadFleet -> emitSource(
                observeFleetRetrieval(Action.ApplyFilter.NoFilter)
            )
            is Action.ScooterUserSelection -> emit(
                Result.MapSelectionResult(
                    action.selection, internalViewState.fleet
                )
            )
            is Action.ApplyFilter -> emitSource(
                observeFleetRetrieval(action)
            )
        }
    }

    override fun reduce(result: Result): ViewState {
        return when (result) {
            is Result.Loading -> internalViewState.copy(isLoading = true)
            is Result.Empty -> internalViewState.copy(isLoading = false)
            is Result.Success -> internalViewState.copy(
                isLoading = false,
                fleet = result.fleetData
            )
            is Result.Failure -> {
                effect.postValue(convertErrorResultToEffect(result))
                internalViewState.copy(
                    isLoading = false,
                    fleet = listOf()
                )
            }
            is Result.MapSelectionResult -> {
                effect.postValue(
                    Effect.OpenMap(
                        result.selection,
                        result.fleet
                    )
                )
                internalViewState
            }

        }
    }

    private fun convertErrorResultToEffect(result: Result.Failure): Effect.ErrorEffect {
        return when (result) {
            is Result.Failure.NetworkFailure -> Effect.ErrorEffect.NetworkError
            is Result.Failure.UnExpectedResult -> Effect.ErrorEffect.UnexpectedError(result.message)
        }
    }


    private suspend fun observeFleetRetrieval(filter: Action.ApplyFilter) = Transformations.map(
        retrieveFleetUseCase.execute(
            RetrieveFleetUseCase.Input(convertToFilter(filter))
        )
    ) { mapToResult(it) }

    private fun convertToFilter(filter: Action.ApplyFilter): RetrieveFleetUseCase.Input.FleetFilter {
        return when (filter) {
            is Action.ApplyFilter.NoFilter -> RetrieveFleetUseCase.Input.FleetFilter.None
            is Action.ApplyFilter.BatteryLevel -> RetrieveFleetUseCase.Input.FleetFilter.BatteryFilter(
                filter.level
            )
            is Action.ApplyFilter.StateFilter -> RetrieveFleetUseCase.Input.FleetFilter.FleetStateFilter(
                filter.state
            )
        }
    }


    private fun mapToResult(output: RetrieveFleetUseCase.Output): Result {
        return when (output) {
            is RetrieveFleetUseCase.Output.Loading -> Result.Loading
            is RetrieveFleetUseCase.Output.Success -> Result.Success(output.fleet)
            is RetrieveFleetUseCase.Output.Empty -> Result.Empty
            is RetrieveFleetUseCase.Output.Error -> {
                return when (output) {
                    is RetrieveFleetUseCase.Output.Error.NetworkError -> Result.Failure.NetworkFailure
                    is RetrieveFleetUseCase.Output.Error.UnexpectedError -> Result.Failure.UnExpectedResult(
                        output.throwable.message
                    )
                }
            }
        }
    }


    internal data class ViewState(
        val isLoading: Boolean = true,
        val fleet: List<FleetEntity> = listOf()
    ) : BaseViewState

    internal sealed class Effect {

        data class OpenMap(
            val selection: FleetEntity,
            val fleet: List<FleetEntity>
        ) : Effect()

        sealed class ErrorEffect : Effect() {
            data class UnexpectedError(val errorMessage: String?) : ErrorEffect()
            object NetworkError : ErrorEffect()
        }
    }

    internal sealed class Action : BaseAction {
        object LoadFleet : Action()
        sealed class ApplyFilter : Action() {
            object NoFilter : ApplyFilter()
            data class BatteryLevel(val level: Int) : ApplyFilter()
            data class StateFilter(val state: FleetState) : ApplyFilter()
        }

        data class ScooterUserSelection(val selection: FleetEntity) : Action()
    }

    sealed class Result : BaseResult {
        object Loading : Result()
        data class Success(val fleetData: List<FleetEntity>) : Result()
        object Empty : Result()
        sealed class Failure : Result() {
            object NetworkFailure : Failure()
            data class UnExpectedResult(val message: String?) : Failure()
        }

        data class MapSelectionResult(val selection: FleetEntity, val fleet: List<FleetEntity>) :
            Result()
    }


}
