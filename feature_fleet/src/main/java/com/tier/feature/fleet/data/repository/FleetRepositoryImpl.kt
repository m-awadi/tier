package com.tier.feature.fleet.data.repository

import com.tier.feature.fleet.data.dataSources.FleetRemoteDataSource
import com.tier.feature.fleet.domain.repository.FleetRepository

internal class FleetRepositoryImpl(
    private val fleetRemoteDataSource: FleetRemoteDataSource
) : FleetRepository {

    override suspend fun retrieveFleet() = fleetRemoteDataSource.retrieveFleet()
}
