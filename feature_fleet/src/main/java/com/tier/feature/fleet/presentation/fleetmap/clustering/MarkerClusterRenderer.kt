package com.tier.feature.fleet.presentation.fleetmap.clustering

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.tier.feature.fleet.domain.model.FleetEntity
import com.tier.feature.fleet.presentation.toLatLng
import com.google.maps.android.clustering.Cluster
import com.tier.feature.fleet.R


class MarkerClusterRenderer(
    context: Context?,
    private val googleMap: GoogleMap,
    clusterManager: ClusterManager<FleetItem>?
) : DefaultClusterRenderer<FleetItem>(context, googleMap, clusterManager),
    GoogleMap.OnCameraMoveListener {

    override fun onCameraMove() {
        currentZoomLevel = googleMap.cameraPosition.zoom
    }

    var currentZoomLevel: Float = 0f
    var maxZoomLevel: Float = googleMap.maxZoomLevel

    override fun onBeforeClusterItemRendered(
        item: FleetItem,
        markerOptions: MarkerOptions
    ) {
        generateMarker(item.fleetEntity, markerOptions)

    }

    override fun onClusterItemRendered(clusterItem: FleetItem, marker: Marker) {
        marker.tag = clusterItem.fleetEntity
    }

    private fun generateMarker(
        fleetEntity: FleetEntity,
        markerOptions: MarkerOptions
    ): MarkerOptions {
        return markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.scooter_map_ic))
            .anchor(0.5f, 0.5f)
            .position(fleetEntity.location.toLatLng())
    }


    override fun shouldRenderAsCluster(cluster: Cluster<FleetItem>): Boolean {
        return cluster.size >= 2 && maxZoomLevel != currentZoomLevel
    }

}
