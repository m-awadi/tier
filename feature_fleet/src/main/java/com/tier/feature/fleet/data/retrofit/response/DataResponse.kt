package com.tier.feature.fleet.data.retrofit.response

import com.google.gson.annotations.SerializedName

data class DataResponse(@SerializedName("current")
                val currentResponse: List<CurrentItemResponse>)