package com.tier.feature.fleet.data.retrofit.response

import com.google.gson.annotations.SerializedName

data class ScootersResponse(@SerializedName("data")
                            val dataResponse: DataResponse)