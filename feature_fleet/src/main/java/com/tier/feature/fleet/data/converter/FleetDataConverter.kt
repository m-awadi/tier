package com.tier.feature.fleet.data.converter

import com.tier.feature.fleet.data.retrofit.response.ScootersResponse
import com.tier.feature.fleet.domain.model.FleetEntity

interface FleetDataConverter {
    fun convert(scootersResponse:ScootersResponse):List<FleetEntity>
}
