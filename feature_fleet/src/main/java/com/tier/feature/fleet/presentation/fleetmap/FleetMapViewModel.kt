package com.tier.feature.fleet.presentation.fleetmap

import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.tier.base.liveData.SingleLiveEvent
import com.tier.base.viewmodel.BaseAction
import com.tier.base.viewmodel.BaseResult
import com.tier.base.viewmodel.BaseViewModel
import com.tier.base.viewmodel.BaseViewState
import com.tier.feature.fleet.domain.model.FleetEntity
import com.tier.feature.fleet.domain.model.FleetState
import com.tier.feature.fleet.presentation.fleetmap.FleetMapViewModel.Action
import com.tier.feature.fleet.presentation.fleetmap.FleetMapViewModel.ViewState

internal class FleetMapViewModel :
    BaseViewModel<ViewState, Action, FleetMapViewModel.Result>() {

    internal data class ViewState(
        val fleet: List<FleetEntity> = listOf()
    ) : BaseViewState


    internal sealed class Effects {
        data class MoveFocusEffect(val selectedScooter: FleetEntity) : Effects()
    }

    val effects = SingleLiveEvent<Effects>()

    internal sealed class Result : BaseResult {
        data class DisplayFleetResult(val fleet: List<FleetEntity>) :
            Result()

        data class MoveFocusResult(val selectedScooter: FleetEntity) : Result()
    }

    internal sealed class Action : BaseAction {
        data class StartLoading(val selectedScooter: FleetEntity, val fleet: List<FleetEntity>) :
            Action()

        data class ScooterSelected(val selectedScooter: FleetEntity) :
            Action()
    }


    override var internalViewState: ViewState = ViewState()

    override fun handle(action: Action) = liveData(viewModelScope.coroutineContext) {
        when (action) {
            is Action.StartLoading -> {
                emit(
                    Result.DisplayFleetResult(action.fleet)
                )
                emit(
                    Result.MoveFocusResult(action.selectedScooter)
                )
            }
            is Action.ScooterSelected -> emit(
                Result.MoveFocusResult(action.selectedScooter)
            )

        }
    }

    override fun reduce(result: Result): ViewState {
        return when (result) {
            is Result.DisplayFleetResult -> internalViewState.copy(
                fleet = result.fleet
            )
            is Result.MoveFocusResult -> {
                effects.postValue(Effects.MoveFocusEffect(result.selectedScooter))
                internalViewState
            }
        }
    }
}
