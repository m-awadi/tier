package com.tier.feature.fleet.data.retrofit.service

import com.tier.feature.fleet.data.retrofit.response.ScootersResponse
import retrofit2.http.GET
import retrofit2.http.Query

internal interface TierRetrofitService {
    @GET("/bins/k2srm")
    suspend fun getScootersFleet(
    ): ScootersResponse
}
