package com.tier.feature.fleet.data.converter

import com.tier.feature.fleet.data.retrofit.response.ScootersResponse
import com.tier.feature.fleet.domain.model.FleetEntity
import com.tier.feature.fleet.domain.model.FleetState
import com.tier.feature.fleet.domain.model.LocationEntity

class FleetDataRemoteConverter : FleetDataConverter {
    override fun convert(scootersResponse: ScootersResponse): List<FleetEntity> {
        return scootersResponse.dataResponse.currentResponse.map {
            FleetEntity(
                id = it.id,
                location = LocationEntity(
                    latitude = it.latitude,
                    longitude = it.longitude
                ),
                state = convertFleetType(it.state),
                battery = it.battery,
                model = it.model
            )
        }
    }

     fun convertFleetType(fleetType: String) = when (fleetType) {
        "PICKED_UP" -> FleetState.PICKED_UP
        "DAMAGED" -> FleetState.DAMAGED
        "ACTIVE" -> FleetState.ACTIVE
        "TO_PICKUP" -> FleetState.TO_PICKUP
        "MAINTENANCE" -> FleetState.MAINTENANCE
        "GPS_ISSUE" -> FleetState.GPS_ISSUE
        else -> FleetState.UNKNOWN //forward compatibility
    }
}
