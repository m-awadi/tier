package com.tier.feature.fleet.presentation.fleetmap

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.maps.android.clustering.ClusterManager
import com.tier.base.extension.observe
import com.tier.feature.fleet.domain.model.FleetEntity
import com.tier.feature.fleet.presentation.fleetmap.clustering.FleetItem
import com.tier.feature.fleet.presentation.fleetmap.clustering.MarkerClusterRenderer
import com.tier.feature.fleet.presentation.toLatLng
import org.koin.android.ext.android.inject


internal class FleetMapFragment : SupportMapFragment(), OnMapReadyCallback {

    companion object {
        const val ARG_FLEET_LIST = "ARG_FLEET_LIST"
        const val ARG_FLEET_SELECTION = "ARG_FLEET_SELECTION"
    }

    private lateinit var clusterManager: ClusterManager<FleetItem>
    private val viewModel: FleetMapViewModel by inject()
    private lateinit var map: GoogleMap

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        getMapAsync(this)
    }

    private val stateObserver = Observer<FleetMapViewModel.ViewState> {

        cleanFleetDisplay(it.fleet)

    }
    private val effectObserver = Observer<FleetMapViewModel.Effects> {

        when (it) {
            is FleetMapViewModel.Effects.MoveFocusEffect -> focusOnScooter(it.selectedScooter)
        }


    }

    private fun focusOnScooter(selection: FleetEntity) {
        map.animateCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.Builder()
                    .target(selection.location.toLatLng())
                    .zoom(20f).build()
            )
        )
    }

    private fun cleanFleetDisplay(fleetList: List<FleetEntity>) {
        clusterManager.clearItems()
        clusterManager.addItems(fleetList.map { FleetItem(it) })
        clusterManager.cluster()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            val selectedScooter =
                arguments?.getParcelable<FleetEntity>(ARG_FLEET_SELECTION)
                    ?: throw IllegalArgumentException("missing selected scooter!")

            val fleet =
                arguments?.getParcelableArrayList<FleetEntity>(ARG_FLEET_LIST)
                    ?: throw IllegalArgumentException("missing fleet!")

            viewModel.dispatch(
                FleetMapViewModel.Action.StartLoading(
                    selectedScooter,
                    fleet.toList()
                )
            )
        }

    }


    override fun onMapReady(googleMap: GoogleMap) {

        map = googleMap
        setupMapDefaults(map)
        setupCluster()
        observe(viewModel.viewState, stateObserver)
        observe(viewModel.effects, effectObserver)
    }

    private fun setupCluster() {
        clusterManager = ClusterManager(requireContext(), map)
        val renderer = MarkerClusterRenderer(requireContext(), map, clusterManager)
        clusterManager.renderer = renderer
        map.setOnCameraIdleListener(clusterManager)
        map.setOnCameraMoveListener(renderer)
    }


    private fun setupMapDefaults(map: GoogleMap) {
        with(map.uiSettings) {
            isZoomControlsEnabled = true
            isMapToolbarEnabled = false
            isRotateGesturesEnabled = true
            isMapToolbarEnabled = false
            isTiltGesturesEnabled = true
            isCompassEnabled = true
        }
        map.setOnMarkerClickListener {
            if (it.tag is FleetEntity) {
                viewModel.dispatch(FleetMapViewModel.Action.ScooterSelected((it.tag as FleetEntity)))
                true
            } else {
                false
            }

        }
    }

}

