package com.tier.feature.fleet.presentation.fleetList

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.pawegio.kandroid.visible
import com.tier.base.extension.observe
import com.tier.base.extension.setOnDebouncedClickListener
import com.tier.base.fragment.BaseFragment
import com.tier.feature.fleet.R
import com.tier.feature.fleet.domain.model.FleetState
import com.tier.feature.fleet.injectFeature
import com.tier.feature.fleet.presentation.fleetList.recyclerview.FleetAdapter
import com.tier.feature.fleet.presentation.fleetmap.FleetMapFragment
import kotlinx.android.synthetic.main.fragment_fleet_list.*
import org.koin.android.ext.android.inject


class FleetListFragment : BaseFragment() {

    private val viewModel: FleetListViewModel by inject()

    override val layoutResourceId = R.layout.fragment_fleet_list

    private val fleetAdapter: FleetAdapter by lazy { FleetAdapter() }

    private val stateObserver = Observer<FleetListViewModel.ViewState> { viewState ->
        fleetAdapter.submitList(viewState.fleet)
        recyclerView.scheduleLayoutAnimation()
        swipeRefresh.isRefreshing = viewState.isLoading
        progress_bar.visible = viewState.isLoading && viewState.fleet.isEmpty()

    }

    private val effectObserver = Observer<FleetListViewModel.Effect> {

        when (it) {
            is FleetListViewModel.Effect.ErrorEffect -> {
                showError(it)
            }
            is FleetListViewModel.Effect.OpenMap -> {
                val bundle = bundleOf(
                    FleetMapFragment.ARG_FLEET_SELECTION to it.selection,
                    FleetMapFragment.ARG_FLEET_LIST to ArrayList(it.fleet)
                )
                findNavController().navigate(
                    com.tier.R.id.action_fleet_list_to_fleetmap,
                    bundle
                )
            }
        }


    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.home_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.filter) {
            showFiltersBottomSheet()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }


    private fun showFiltersBottomSheet() {
        val bottomSheet = BottomSheetDialog(requireContext())
        val layoutDialog =
            layoutInflater.inflate(R.layout.dialog_scooters_filter, view as ViewGroup?, false)
        bottomSheet.setContentView(layoutDialog)

        layoutDialog.findViewById<TextView>(R.id.no_filter).setOnDebouncedClickListener {
            viewModel.dispatch(FleetListViewModel.Action.ApplyFilter.NoFilter as FleetListViewModel.Action)
            bottomSheet.dismiss()
        }
        layoutDialog.findViewById<TextView>(R.id.to_pickup).setOnDebouncedClickListener {
            viewModel.dispatch(
                FleetListViewModel.Action.ApplyFilter.StateFilter(
                    FleetState.TO_PICKUP
                ))
            bottomSheet.dismiss()
        }
        layoutDialog.findViewById<TextView>(R.id.damaged).setOnDebouncedClickListener {
            viewModel.dispatch(FleetListViewModel.Action.ApplyFilter.StateFilter(
                FleetState.DAMAGED
            ))
            bottomSheet.dismiss()
        }
        layoutDialog.findViewById<TextView>(R.id.below50).setOnDebouncedClickListener {
            viewModel.dispatch(FleetListViewModel.Action.ApplyFilter.BatteryLevel(
                49
            ))
            bottomSheet.dismiss()
        }

        bottomSheet.show()
    }

    private fun showError(errorType: FleetListViewModel.Effect.ErrorEffect) {
        val error = when (errorType) {
            is FleetListViewModel.Effect.ErrorEffect.NetworkError -> getString(R.string.error_network_error)
            is FleetListViewModel.Effect.ErrorEffect.UnexpectedError -> if (errorType.errorMessage.isNullOrEmpty()) {
                getString(R.string.error_unable_to_load_list)
            } else {
                errorType.errorMessage
            }

        }


        Snackbar.make(requireView(), error, Snackbar.LENGTH_LONG)
            .show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectFeature()
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fleetAdapter.setOnDebouncedClickListener {
            viewModel.dispatch(FleetListViewModel.Action.ScooterUserSelection(it))
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this.context)
            setHasFixedSize(true)
            adapter = fleetAdapter
        }

        observe(viewModel.viewState, stateObserver)
        observe(viewModel.effect, effectObserver)

        if (fleetAdapter.itemCount == 0) {
            loadItems()
        }

        swipeRefresh.setOnRefreshListener {
            loadItems()
        }
    }

    private fun loadItems() {
        viewModel.dispatch(
            FleetListViewModel.Action.LoadFleet
        )
    }


}
