package com.tier.feature.fleet.presentation

import com.google.android.gms.maps.model.LatLng
import com.tier.feature.fleet.domain.model.LocationEntity

fun LocationEntity.toLatLng(): LatLng {
    return LatLng(this.latitude, this.longitude)
}
