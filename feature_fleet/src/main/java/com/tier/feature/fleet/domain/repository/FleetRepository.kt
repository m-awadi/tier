package com.tier.feature.fleet.domain.repository

import com.tier.feature.fleet.domain.model.FleetEntity

internal interface FleetRepository {

    suspend fun retrieveFleet(): List<FleetEntity>
}
