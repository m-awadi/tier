package com.tier.feature.fleet.domain.model


enum class FleetState {
    PICKED_UP, DAMAGED, ACTIVE, TO_PICKUP, MAINTENANCE, GPS_ISSUE,UNKNOWN
}
