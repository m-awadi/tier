package com.tier.feature.fleet.data.retrofit.response

import com.google.gson.annotations.SerializedName

data class CurrentItemResponse(
    @SerializedName("resolvedAt")
    val resolvedAt: String? = null,
    @SerializedName("resolvedBy")
    val resolvedBy: String? = null,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("battery")
    val battery: Int,
    @SerializedName("resolution")
    val resolution: String?= null,
    @SerializedName("model")
    val model: String ,
    @SerializedName("id")
    val id: String,
    @SerializedName("vehicleId")
    val vehicleId: String,
    @SerializedName("state")
    val state: String ,
    @SerializedName("fleetbirdId")
    val fleetbirdId: String,
    @SerializedName("longitude")
    val longitude: Double
)