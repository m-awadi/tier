
package com.tier.feature.fleet.presentation.fleetmap.clustering

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import com.tier.feature.fleet.domain.model.FleetEntity
import com.tier.feature.fleet.presentation.toLatLng

data class FleetItem(val fleetEntity: FleetEntity) : ClusterItem {
    override fun getPosition(): LatLng {
        return fleetEntity.location.toLatLng()
    }

    override fun getTitle(): String {
        return fleetEntity.state.name
    }

    override fun getSnippet(): String? {
        return null
    }

}
