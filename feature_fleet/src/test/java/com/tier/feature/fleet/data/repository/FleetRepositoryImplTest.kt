package com.tier.feature.fleet.data.repository

import com.tier.feature.fleet.data.dataSources.FleetRemoteDataSource
import com.tier.feature.fleet.domain.DomainFixtures
import com.tier.feature.fleet.domain.DomainFixtures.jsonSample
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.`should be equal to`
import org.junit.Before
import org.junit.Test


class FleetRepositoryImplTest {

    private val mockRemoteDataSource: FleetRemoteDataSource = mockk()

    private lateinit var cut: FleetRepositoryImpl

    @Before
    fun setUp() {
        cut = FleetRepositoryImpl(mockRemoteDataSource)
        clearAllMocks()
    }

    @Test
    fun `retrieveFleet should call remote data source and pass the data to repo`() {
        val fixture = DomainFixtures.getFleetPOIList(jsonSample)
        // given
        val result = runBlocking {
            coEvery {
                mockRemoteDataSource.retrieveFleet(
                )
            } returns DomainFixtures.getFleetPOIList(jsonSample)

            // when
            cut.retrieveFleet(
            )


        }
        // then
        result `should be equal to` fixture
    }
}
