package com.tier.feature.fleet.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.liveData
import com.jraska.livedata.test
import com.tier.feature.fleet.domain.usecase.RetrieveFleetUseCase
import com.tier.feature.fleet.presentation.fleetList.FleetListViewModel
import com.tier.library.testutils.CoroutineRule
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class FleetListViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutineTestRule = CoroutineRule()

    @get:Rule
    val testRule = InstantTaskExecutorRule()

    private val retrieveFleetUseCase: RetrieveFleetUseCase = mockk()

    private lateinit var cut: FleetListViewModel

    @Before
    fun setUp() {
        cut = FleetListViewModel(retrieveFleetUseCase)
        clearAllMocks()
    }

    @Test
    fun `when viewmodel dispatched initially, it should omit the all fleet result`() {

        runBlocking {
            val testObserver = cut.viewState.test()
            //given
            val fleet =
                PresentationFixtures.getFleetPOIList()
            coEvery {
                retrieveFleetUseCase.execute(
                    RetrieveFleetUseCase.Input()
                )
            } returns liveData {
                emit(RetrieveFleetUseCase.Output.Loading)
                emit(RetrieveFleetUseCase.Output.Success(fleet))
            }

            //when
            cut.dispatch(
                FleetListViewModel.Action.LoadFleet
            )


            //then
            testObserver.assertHasValue()
                .assertValueHistory(
                    FleetListViewModel.ViewState(isLoading = true)
                    , FleetListViewModel.ViewState(
                        isLoading = false,
                        fleet = fleet
                    )
                )

        }
    }


    @Test
    fun `when usecase returns empty result, it should update view state with empty list `() {
        runBlocking {
            val testObserver = cut.viewState.test()
            //given
            coEvery {
                retrieveFleetUseCase.execute(
                    RetrieveFleetUseCase.Input()
                )
            } returns liveData {
                emit(RetrieveFleetUseCase.Output.Loading)
                emit(RetrieveFleetUseCase.Output.Empty)
            }

            //when
            cut.dispatch(
                FleetListViewModel.Action.LoadFleet
            )


            //then
            testObserver.assertHasValue()
                .assertValueHistory(
                    FleetListViewModel.ViewState(isLoading = true)
                    , FleetListViewModel.ViewState(
                        isLoading = false,
                        fleet = listOf()
                    )
                )

        }
    }

    @Test
    fun `when usecase return an error, viewmodel should update view state with the error`() {

        runBlocking {
            val viewStateTestObserver = cut.viewState.test()
            val effectTestObserver = cut.effect.test()
            //given
            coEvery {
                retrieveFleetUseCase.execute(
                    RetrieveFleetUseCase.Input()
                )
            } returns liveData {
                emit(RetrieveFleetUseCase.Output.Loading)
                emit(RetrieveFleetUseCase.Output.Error.NetworkError)
            }

            //when
            cut.dispatch(
                FleetListViewModel.Action.LoadFleet
            )


            //then
            viewStateTestObserver.assertHasValue()
                .assertValueHistory(
                    FleetListViewModel.ViewState(isLoading = true)
                    , FleetListViewModel.ViewState(
                        isLoading = false,
                        fleet = listOf()
                    )
                )

            effectTestObserver.assertHasValue()
                .assertValueHistory(
                    FleetListViewModel.Effect.ErrorEffect.NetworkError
                )

        }
    }

    @Test
    fun `when user select a fleet viewmodel should forward the request to effect with corresponding selection to react`() {

        runBlocking {
            val effectTestObserver = cut.effect.test()
            val viewStateTestObserver = cut.viewState.test()

            val fleet =PresentationFixtures.getFleetPOIList()

            //given
            coEvery {
                retrieveFleetUseCase.execute(
                    RetrieveFleetUseCase.Input()
                )
            } returns liveData {
                emit(RetrieveFleetUseCase.Output.Loading)
                emit(RetrieveFleetUseCase.Output.Success(fleet))
            }

            //when
            cut.dispatch(
                FleetListViewModel.Action.LoadFleet
            )//load screen
            cut.dispatch(FleetListViewModel.Action.ScooterUserSelection(fleet[0]))//select a scooter


            //then
            viewStateTestObserver.assertHasValue()
                .assertValueHistory(
                    FleetListViewModel.ViewState(isLoading = true)
                    , FleetListViewModel.ViewState(
                        isLoading = false,
                        fleet = fleet
                    )
                    , FleetListViewModel.ViewState(
                        isLoading = false,
                        fleet = fleet
                    )
                )
            effectTestObserver.assertHasValue()
                .assertValueHistory(
                    FleetListViewModel.Effect.OpenMap(fleet[0], fleet)
                )

        }
    }
}
