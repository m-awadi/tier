package com.tier.feature.fleet.data

import com.google.gson.Gson
import com.tier.feature.fleet.data.retrofit.response.CurrentItemResponse
import com.tier.feature.fleet.data.retrofit.response.DataResponse
import com.tier.feature.fleet.data.retrofit.response.ScootersResponse

object DataFixtures {


    fun getDummyPOIResponse() =
        ScootersResponse(
            DataResponse(
                listOf(
                    Gson().fromJson(
                        """
                        {
        "id": "7130e0d5-b1d1-42ac-9c27-0b367036adee",
        "vehicleId": "e4eea1ba-71cf-412b-94d2-0a4987d84faf",
        "hardwareId": "c53426798e7727d9",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "Gb7ChtSxVrMsOe3WThOtCiFz1Hn2",
        "resolvedAt": "2019-10-15T06:53:14.342Z",
        "battery": 0,
        "state": "MAINTENANCE",
        "model": "UNKNOWN",
        "fleetbirdId": 19588,
        "latitude": 57.643016,
        "longitude": 12.028591
      }
                    """.trimIndent()
                        , CurrentItemResponse::class.java
                    )
                )
            )

        )
}
