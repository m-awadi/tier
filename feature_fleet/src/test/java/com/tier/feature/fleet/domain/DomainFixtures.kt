package com.tier.feature.fleet.domain

import com.google.gson.Gson
import com.tier.feature.fleet.data.converter.FleetDataRemoteConverter
import com.tier.feature.fleet.data.retrofit.response.ScootersResponse
import com.tier.feature.fleet.domain.model.FleetEntity

object DomainFixtures {
    val converter = FleetDataRemoteConverter()


     fun getFleetPOIList(json: String) =
        converter.convert(
            Gson().fromJson(
                json, ScootersResponse::class.java
            )
        )

    val getEmptyFleetPOIList = listOf<FleetEntity>()

    val fourSatausJson = """
                  {
  "data": {
    "current": [
        {
        "id": "8a8b5e12-ebc1-4d7c-8513-87954d13c057",
        "vehicleId": "dfdfd610-13f8-4148-9bf1-520800d7ab5c",
        "hardwareId": "868446034677159",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "3uQVX5Fst4bRO7PNb9Dx0lqRzRf2",
        "resolvedAt": "2019-10-15T08:04:04.478Z",
        "battery": 82,
        "state": "PICKED_UP",
        "model": "AA",
        "fleetbirdId": 107693,
        "latitude": 57.711268,
        "longitude": 11.988065
      },
      {
        "id": "d76d9ceb-800a-4fe5-84f7-ec354387cb2e",
        "vehicleId": "5d4275f5-f028-473b-88a6-e944f9e47286",
        "hardwareId": "868446034597555",
        "zoneId": "GOTHENBURG",
        "battery": 78,
        "state": "TO_PICKUP",
        "model": "AA",
        "fleetbirdId": 107180,
        "latitude": 57.692821,
        "longitude": 11.939658
      },
      {
        "id": "de7927e5-3dfc-46d0-8faf-a25a962159bf",
        "vehicleId": "ff0d2f91-5b1c-4d56-b3b6-9d120a64f897",
        "hardwareId": "867584031520401",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "Gb7ChtSxVrMsOe3WThOtCiFz1Hn2",
        "resolvedAt": "2019-10-15T05:49:03.089Z",
        "battery": 7,
        "state": "MAINTENANCE",
        "model": "UNKNOWN",
        "fleetbirdId": 20352,
        "latitude": 57.6430036666667,
        "longitude": 12.0284381666667
      },
      {
        "id": "3de89876-bed6-47e7-9deb-7f85df7a2ca9",
        "vehicleId": "bcf7ee32-e0e9-4b34-9872-62d3edfe1038",
        "hardwareId": "868446034517629",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "vAnYZHOw1ObinSH7eiFfykyxuv82",
        "resolvedAt": "2019-10-15T10:25:39.719Z",
        "battery": 99,
        "state": "PICKED_UP",
        "model": "AA",
        "fleetbirdId": 106606,
        "latitude": 57.702998,
        "longitude": 11.962626
      },
      {
        "id": "cb171b56-97e7-4178-a318-c3500b442d31",
        "vehicleId": "2fe63090-e697-4511-9601-ade5f7783b64",
        "hardwareId": "867584031375376",
        "zoneId": "GOTHENBURG",
        "resolution": "NOT_FOUND",
        "resolvedBy": "Gb7ChtSxVrMsOe3WThOtCiFz1Hn2",
        "resolvedAt": "2019-10-15T09:34:24.472Z",
        "battery": 97,
        "state": "GPS_ISSUE",
        "model": "UNKNOWN",
        "fleetbirdId": 22725,
        "latitude": 57.649055,
        "longitude": 12.1218115
      },
      {
        "id": "3523552d-8734-432b-b4bc-20826caf3d4d",
        "vehicleId": "0888607a-d961-4f7d-bb4d-ae3deb6e9e9f",
        "hardwareId": "868446034628798",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "vAnYZHOw1ObinSH7eiFfykyxuv82",
        "resolvedAt": "2019-10-15T08:30:48.311Z",
        "battery": 58,
        "state": "PICKED_UP",
        "model": "AA",
        "fleetbirdId": 102717,
        "latitude": 57.703027,
        "longitude": 11.962388
      },
      {
        "id": "f5cd6560-0fdd-4db7-8bac-b97dfe86e439",
        "vehicleId": "215a23a2-e9bb-4523-a962-1f03b7230fdc",
        "hardwareId": "868446034639340",
        "zoneId": "GOTHENBURG",
        "battery": 57,
        "state": "TO_PICKUP",
        "model": "AA",
        "fleetbirdId": 105301,
        "latitude": 57.697554,
        "longitude": 11.940634
      },
      {
        "id": "711aef15-11a2-4d96-9bd3-3c061554c7b6",
        "vehicleId": "545dd55b-c0d8-4f6b-8b4f-87c6df2ac1ad",
        "hardwareId": "867584031303873",
        "zoneId": "GOTHENBURG",
        "battery": 49,
        "state": "TO_PICKUP",
        "model": "UNKNOWN",
        "fleetbirdId": 21303,
        "latitude": 57.6883991666667,
        "longitude": 11.9062688333333
      },
      {
        "id": "53e13558-1707-406f-b20f-68d0d59a63e5",
        "vehicleId": "4ead2cf8-1128-4537-b455-9b8601133a40",
        "hardwareId": "868446034517967",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "vAnYZHOw1ObinSH7eiFfykyxuv82",
        "resolvedAt": "2019-10-15T10:25:43.723Z",
        "battery": 90,
        "state": "PICKED_UP",
        "model": "AA",
        "fleetbirdId": 106338,
        "latitude": 57.702975,
        "longitude": 11.962797
      },
      {
        "id": "42640943-d4fd-47cd-95f6-21c73fc34408",
        "vehicleId": "302bb363-f9fc-4553-aca8-20da8e035462",
        "hardwareId": "868446034564324",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "3uQVX5Fst4bRO7PNb9Dx0lqRzRf2",
        "resolvedAt": "2019-10-15T07:46:42.683Z",
        "battery": 96,
        "state": "PICKED_UP",
        "model": "AA",
        "fleetbirdId": 105321,
        "latitude": 57.71212,
        "longitude": 11.990187
      },
      {
        "id": "a6d3ebf1-1b64-4135-b6e1-688f4cf70df0",
        "vehicleId": "3844ff96-4e9c-4c71-b249-98918733eff8",
        "hardwareId": "867584031517928",
        "zoneId": "GOTHENBURG",
        "battery": 55,
        "state": "TO_PICKUP",
        "model": "UNKNOWN",
        "fleetbirdId": 22387,
        "latitude": 57.693507833333335,
        "longitude": 11.9419145
      },
      {
        "id": "f8302b37-4339-488a-ac8d-1e41619a975c",
        "vehicleId": "b8f12cd0-9d5c-41e0-8068-35c83a83a774",
        "hardwareId": "867584031321768",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "vAnYZHOw1ObinSH7eiFfykyxuv82",
        "resolvedAt": "2019-10-15T09:10:21.525Z",
        "battery": 53,
        "state": "PICKED_UP",
        "model": "UNKNOWN",
        "fleetbirdId": 22543,
        "latitude": 57.7030795,
        "longitude": 11.9627543333333
      },
      {
        "id": "f8835332-66e6-4215-9b23-46008634ab38",
        "vehicleId": "592e7bef-5985-444f-ab2e-036d7fafb017",
        "hardwareId": "868446034613113",
        "zoneId": "GOTHENBURG",
        "battery": 58,
        "state": "TO_PICKUP",
        "model": "AA",
        "fleetbirdId": 100720,
        "latitude": 57.694836,
        "longitude": 11.913445
      }
      
       ],
    "stats": {
      "open": 26,
      "assigned": 0,
      "resolved": 60
    }
  }
}
    """.trimIndent()


     val jsonSample = """
{
  "data": {
    "current": [
      {
        "id": "7130e0d5-b1d1-42ac-9c27-0b367036adee",
        "vehicleId": "e4eea1ba-71cf-412b-94d2-0a4987d84faf",
        "hardwareId": "c53426798e7727d9",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "Gb7ChtSxVrMsOe3WThOtCiFz1Hn2",
        "resolvedAt": "2019-10-15T06:53:14.342Z",
        "battery": 0,
        "state": "MAINTENANCE",
        "model": "UNKNOWN",
        "fleetbirdId": 19588,
        "latitude": 57.643016,
        "longitude": 12.028591
      }
    ],
    "stats": {
      "open": 26,
      "assigned": 0,
      "resolved": 60
    }
  }
}
       
       """
}
