package com.tier.feature.fleet.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.tier.feature.fleet.data.repository.FleetRepositoryImpl
import com.tier.feature.fleet.domain.DomainFixtures
import com.tier.feature.fleet.domain.model.FleetState
import com.tier.library.testutils.CoroutineRule
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okio.IOException
import org.junit.Rule
import org.junit.Test


class RetrieveFleetUseCaseTest {


    private val mockRepo: FleetRepositoryImpl = mockk()
    private val cut: RetrieveFleetUseCase by lazy {
        RetrieveFleetUseCase(mockRepo)
    }

    @get:Rule
    val testRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutineTestRule = CoroutineRule()


    @Test
    fun `when execute with no filter should return all results`() {
        runBlocking {
            // given
            val fleet = DomainFixtures.getFleetPOIList(DomainFixtures.fourSatausJson)

            coEvery {
                mockRepo.retrieveFleet()
            } returns fleet


            // when
            val testObserver = cut.execute(
                RetrieveFleetUseCase.Input()
            ).test()

            // then

            testObserver.assertHasValue()
                .assertValueHistory(
                    RetrieveFleetUseCase.Output.Loading,
                    RetrieveFleetUseCase.Output.Success(
                        fleet
                    )
                )

        }
    }

    @Test
    fun `when execute with low battery filter should return only low battery scooters`() {
        runBlocking {
            // given
            val fleet = DomainFixtures.getFleetPOIList(DomainFixtures.fourSatausJson)

            coEvery {
                mockRepo.retrieveFleet()
            } returns fleet


            // when
            val testObserver = cut.execute(
                RetrieveFleetUseCase.Input(RetrieveFleetUseCase.Input.FleetFilter.BatteryFilter(60))
            ).test()

            // then

            testObserver.assertHasValue()
                .assertValueHistory(
                    RetrieveFleetUseCase.Output.Loading,
                    RetrieveFleetUseCase.Output.Success(
                        fleet.filter { it.battery < 60 }
                    )
                )

        }
    }

    @Test
    fun `when execute with state filter should return only corresponding state`() {
        runBlocking {
            // given
            val fleet = DomainFixtures.getFleetPOIList(DomainFixtures.fourSatausJson)

            coEvery {
                mockRepo.retrieveFleet()
            } returns fleet


            // when
            val testObserver = cut.execute(
                RetrieveFleetUseCase.Input(
                    RetrieveFleetUseCase.Input.FleetFilter.FleetStateFilter(
                        FleetState.GPS_ISSUE
                    )
                )
            ).test()

            // then

            testObserver.assertHasValue()
                .assertValueHistory(
                    RetrieveFleetUseCase.Output.Loading,
                    RetrieveFleetUseCase.Output.Success(
                        fleet.filter { it.state == FleetState.GPS_ISSUE }
                    )
                )

        }
    }

    @Test
    fun `when execute when network error occurs, should report network error `() {
        runBlocking {
            // given
            coEvery {
                mockRepo.retrieveFleet()
            } coAnswers { throw IOException() }


            // when
            val testObserver = cut.execute(
                RetrieveFleetUseCase.Input()
            ).test()

            // then

            testObserver.assertHasValue()
                .assertValueHistory(
                    RetrieveFleetUseCase.Output.Loading,
                    RetrieveFleetUseCase.Output.Error.NetworkError
                )

        }
    }

}
