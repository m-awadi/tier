package com.tier.feature.fleet.presentation.fleetmap

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.tier.feature.fleet.presentation.PresentationFixtures
import com.tier.library.testutils.CoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test

class FleetMapViewModelTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutineTestRule = CoroutineRule()

    @get:Rule
    val testRule = InstantTaskExecutorRule()

    private var cut: FleetMapViewModel = FleetMapViewModel()

    @Test
    fun `when start loading it should load the fleet and zoom on selected scooter`() {

        runBlocking {
            val viewStateObserver = cut.viewState.test()
            val effectsObserver = cut.effects.test()

            //given
            val fleet = PresentationFixtures.getFleetPOIList()
            val selected = fleet[0]

            //when
            cut.dispatch(FleetMapViewModel.Action.StartLoading(selected, fleet))

            //then
            viewStateObserver.assertHasValue()
                .assertValueHistory(
                    FleetMapViewModel.ViewState(
                        fleet
                    ),
                    FleetMapViewModel.ViewState(
                        fleet
                    )
                )
            effectsObserver.assertHasValue()
                .assertValueHistory(
                    FleetMapViewModel.Effects.MoveFocusEffect(
                        selected
                    )
                )

        }
    }

    @Test
    fun `when user click scooter it should move the focus to the selected scooter`() {

        runBlocking {
            val viewStateObserver = cut.viewState.test()
            val effectsObserver = cut.effects.test()

            //given
            val fleet = PresentationFixtures.getFleetPOIList()
            val selected = fleet[0]
            val secondSelection = fleet[1]

            //when
            cut.dispatch(FleetMapViewModel.Action.StartLoading(selected, fleet))
            cut.dispatch(FleetMapViewModel.Action.ScooterSelected(secondSelection))

            //then
            viewStateObserver.assertHasValue()
                .assertValueHistory(
                    FleetMapViewModel.ViewState(
                        fleet
                    ),
                    FleetMapViewModel.ViewState(
                        fleet
                    ),
                    FleetMapViewModel.ViewState(
                        fleet
                    )
                )
            effectsObserver.assertHasValue()
                .assertValueHistory(
                    FleetMapViewModel.Effects.MoveFocusEffect(
                        selected
                    ),
                    FleetMapViewModel.Effects.MoveFocusEffect(
                        secondSelection
                    )
                )

        }
    }

}
