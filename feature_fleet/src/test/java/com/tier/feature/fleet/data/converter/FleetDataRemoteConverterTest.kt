package com.tier.feature.fleet.data.converter

import com.tier.feature.fleet.data.retrofit.response.CurrentItemResponse
import com.tier.feature.fleet.data.retrofit.response.DataResponse
import com.tier.feature.fleet.data.retrofit.response.ScootersResponse
import com.tier.feature.fleet.domain.model.FleetEntity
import com.tier.feature.fleet.domain.model.FleetState
import com.tier.feature.fleet.domain.model.LocationEntity
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.`should be equal to`
import org.junit.Test


class FleetDataRemoteConverterTest {

    val service = FleetDataRemoteConverter()
    @Test
    fun convertFleetType() {
        service.convertFleetType("PICKED_UP") `should be equal to` FleetState.PICKED_UP
        service.convertFleetType("DAMAGED") `should be equal to` FleetState.DAMAGED
        service.convertFleetType("ACTIVE") `should be equal to` FleetState.ACTIVE
        service.convertFleetType("TO_PICKUP") `should be equal to` FleetState.TO_PICKUP
        service.convertFleetType("MAINTENANCE") `should be equal to` FleetState.MAINTENANCE
        service.convertFleetType("GPS_ISSUE") `should be equal to` FleetState.GPS_ISSUE
        service.convertFleetType("NEW_STATUS") `should be equal to` FleetState.UNKNOWN

    }

    @Test
    fun convert() {
        runBlocking {

            val response = ScootersResponse(
                DataResponse(
                    listOf(
                        CurrentItemResponse(
                            id = "id", longitude = 1994.3,
                            latitude = 33.3,
                            state = "ACTIVE",
                            battery = 59,
                            model = "tesla",
                            vehicleId = "teslaId",
                            fleetbirdId = "44949"
                        )
                    )
                )
            )

            service.convert(response) `should be equal to` listOf(
                FleetEntity(
                    "id", FleetState.ACTIVE,
                    LocationEntity(33.3,1994.3), 59,
                    "tesla"
                )
            )
        }
    }


}