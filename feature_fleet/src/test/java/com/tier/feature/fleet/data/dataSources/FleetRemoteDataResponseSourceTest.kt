package com.tier.feature.fleet.data.dataSources

import com.tier.feature.fleet.data.DataFixtures
import com.tier.feature.fleet.data.converter.FleetDataRemoteConverter
import com.tier.feature.fleet.data.retrofit.service.TierRetrofitService
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.`should be equal to`
import org.junit.Before
import org.junit.Test


class FleetRemoteDataResponseSourceTest {

    private var tierRetrofitService: TierRetrofitService = mockk()

    val converter = FleetDataRemoteConverter()
    private val cut: FleetRemoteDataSource by lazy {
        FleetRemoteDataSource(tierRetrofitService, converter)
    }

    @Before
    fun setup() {
        clearAllMocks()
    }

    @Test
    fun `retrieve fleet should return the associate domain model`() {
        runBlocking {

            val fleetPOIResponse = DataFixtures.getDummyPOIResponse()

            coEvery {
                tierRetrofitService.getScootersFleet(

                )
            } returns fleetPOIResponse

            val result = cut.retrieveFleet(

            )

            result `should be equal to` converter.convert(fleetPOIResponse)

        }

    }


}
