package com.tier.feature.fleet.presentation

import com.google.gson.Gson
import com.tier.feature.fleet.data.converter.FleetDataRemoteConverter
import com.tier.feature.fleet.data.retrofit.response.ScootersResponse
import com.tier.feature.fleet.domain.model.FleetEntity

object PresentationFixtures {
    val converter = FleetDataRemoteConverter()


     fun getFleetPOIList() =
        converter.convert(
            Gson().fromJson(
                jsonSample, ScootersResponse::class.java
            )
        )

    private const val jsonSample = """
     {
  "data": {
    "current": [
      {
        "id": "7130e0d5-b1d1-42ac-9c27-0b367036adee",
        "vehicleId": "e4eea1ba-71cf-412b-94d2-0a4987d84faf",
        "hardwareId": "c53426798e7727d9",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "Gb7ChtSxVrMsOe3WThOtCiFz1Hn2",
        "resolvedAt": "2019-10-15T06:53:14.342Z",
        "battery": 0,
        "state": "MAINTENANCE",
        "model": "UNKNOWN",
        "fleetbirdId": 19588,
        "latitude": 57.643016,
        "longitude": 12.028591
      },
      {
        "id": "ac924e62-608b-48db-8047-11dada2dc8ca",
        "vehicleId": "0386cc06-1cc5-4613-9d66-e6e6c0cc1015",
        "hardwareId": "868446034639134",
        "zoneId": "GOTHENBURG",
        "resolution": "CLAIMED",
        "resolvedBy": "3uQVX5Fst4bRO7PNb9Dx0lqRzRf2",
        "resolvedAt": "2019-10-15T08:40:45.334Z",
        "battery": 81,
        "state": "ACTIVE",
        "model": "AA",
        "fleetbirdId": 107430,
        "latitude": 57.712114,
        "longitude": 11.990033
      }
       ],
    "stats": {
      "open": 26,
      "assigned": 0,
      "resolved": 60
    }
  }
}
     
     """
}
