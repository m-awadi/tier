package com.tier


import com.google.android.play.core.splitcompat.SplitCompatApplication
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class Application : SplitCompatApplication() {

    override fun onCreate() {
        super.onCreate()

        startKoin { androidContext(this@Application) }
        CalligraphyConfig.initDefault(
            CalligraphyConfig.Builder()
                .setDefaultFont(R.font.quicksand_regular)
                .build()
        )
    }
}
